package homework2;

public interface Iterator<E> {
    boolean hasNext();
    E next();
}
